package helloFirst;

interface WeaponBehavior{
    void useWeapon();
}
class SwordBehavior implements WeaponBehavior{
    public void useWeapon(){
        System.out.println("sword");
    }
}
class KinfeBehavior implements WeaponBehavior{
    public void useWeapon(){
        System.out.println("Kinfe");
    }
}
class BowAndArrowBhavior implements WeaponBehavior{
    public void useWeapon(){
        System.out.println("BowAndArrow");
    }
}
class AxeBehavior implements WeaponBehavior{
    public void useWeapon(){
        System.out.println("Axe");
    }
}

abstract class Character{
    private WeaponBehavior weapon;
    Character(){
        weapon = new SwordBehavior();
    }
    public void setWeapon(WeaponBehavior wea){
        this.weapon = wea;
    }
    public void fight(){
        System.out.println("start da");
        weapon.useWeapon();
    }
}
class King extends Character{
    public void fight(){
        super.fight();
    }
}
class kinght extends Character{
    public void fight(){
        super.fight();
    }
}
class Queen extends Character{
    public void fight(){
        super.fight();
    }
}
class Troll extends Character{
    public void fight(){
        super.fight();
    }
}
public class ProblemOne{
    public static void main(String arg[]){
        Queen c = new Queen();
        c.fight();
        c.setWeapon(new AxeBehavior());
        c.fight();
        c.setWeapon(new KinfeBehavior());
        c.fight();
    }
}