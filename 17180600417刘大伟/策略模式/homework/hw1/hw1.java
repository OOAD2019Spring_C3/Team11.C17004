interface WeaponBehavior{
    void useWeapon();
}
class SwordBehavior implements WeaponBehavior{
    @Override
    public void useWeapon(){
        System.out.println("use Sword");
    }
}
class KnifeBehavior implements WeaponBehavior{
    @Override
    public void useWeapon(){
        System.out.println("use Knife");
    }
}
class BowAndArrowBehavior implements WeaponBehavior{
    @Override
    public void useWeapon(){
        System.out.println("use Bow and arrow");
    }
}
class AxeBehavior implements WeaponBehavior{
    @Override
    public void useWeapon(){
        System.out.println("use Axe");
    }
}
abstract class Character{
    private WeaponBehavior weapon;
    Character(){
        weapon = new SwordBehavior();
    }
    public void setWeapon(WeaponBehavior w){
        this.weapon = w;
    }
    public void fight(){
        System.out.print("fight!  ");
        weapon.useWeapon();
    }
}
class King extends Character{
    @Override
    public void fight(){
        super.fight();
    }
}
class Knight extends Character{
    @Override
    public void fight(){
        super.fight();
    }
}
class Queen extends Character{
    @Override
    public void fight(){
        super.fight();
    }
}
class Troll extends Character{
    @Override
    public void fight(){
        super.fight();
    }
}
public class ProblemOne{
    public static void main(String arg[]){
        Queen c = new Queen();
        c.fight();
        c.setWeapon(new AxeBehavior());
        c.fight();
        c.setWeapon(new KnifeBehavior());
        c.fight();
    }
}